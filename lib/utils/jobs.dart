class Jobs {
  final id;
  final img;
  final name;
  final company;
  final location;
  final time_publication;
  final n_request;
  final description;
  final experience;
  final job_type;
  final sector;

  Jobs(
      {this.id,
      this.img,
      this.name,
      this.company,
      this.location,
      this.time_publication,
      this.n_request,
      this.description,
      this.experience,
      this.job_type,
      this.sector});

  factory Jobs.fromJson(Map<String, dynamic> json) => Jobs(
      id: json["id"],
      img: json["img"],
      name: json["name"],
      company: json["company"],
      location: json["location"],
      time_publication: json["time_publication"],
      n_request: json["n_request"],
      description: json["description"],
      experience: json["experience"],
      job_type: json["job_type"],
      sector: json["sector"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "img": img,
        "name": name,
        "company": company,
        "location": location,
        "time_publication": time_publication,
        "n_request": n_request,
        "description": description,
        "experience": experience,
        "job_type": job_type,
        "sector": sector
      };
}
